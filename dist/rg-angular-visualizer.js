(function () {
   'use strict';
   angular.module("rg-angular-visualizer",[])
    .provider("rgVisualizer", function(){

        this.envScript = "";
        this.assetsPath = "";
        this.parentDivID = "";
        this.sceneName = "";

        this.$get = function(){
            var envScript = this.envScript;
            var assetsPath = this.assetsPath;
            var parentDivID = this.parentDivID;
            var sceneName = this.sceneName;
            return {
                GetEnvScript: function(){
                    return envScript;
                },
                GetAssetsPath: function(){
                    return assetsPath;
                },
                GetParentDivID: function(){
                    return parentDivID;
                },
                GetSceneName: function(){
                    return sceneName;
                },
                SetEnvScript: function(script){
                    envScript = script;
                },
                SetAssetsPath: function(path){
                    assetsPath = path;
                },
                SetParentDivID: function(ID){
                    parentDivID = ID;
                },
                SetSceneName: function(name){
                    sceneName = name;
                }
            };
        };


        this.setEnvScript = function(envScript){
            this.envScript = envScript;
        };

        this.setAssetsPath = function (path) {
            this.assetsPath = path;
        };

        this.setParentDivID = function(ID){
            this.parentDivID = ID;
        };


        this.SetEnvScript = function(script){
            this.envScript = script;
        };

        this.setSceneName = function(sceneName){
            this.sceneName = sceneName;
        };
    })
    .service('Visualizer', function($timeout, rgVisualizer){

        this.InitVisualizerEvents = function(){
            document.body.addEventListener("onSceneInitialize", HandleSceneInitializeEvent, false);
            document.body.addEventListener("OnLoadingSceneInitialize", HandleLoadingSceneInitializeEvent, false);
            document.body.addEventListener("onSceneLoad", HandleSceneLoadEvent, false);
            document.body.addEventListener("onGridInit", HandleGridInitEvent, false);
            document.body.addEventListener("onGridInitFinished", HandleGridInitFinishedEvent, false);
            document.body.addEventListener("onPlayBtnClicked", HandlePlayBtnClicked, false);
            document.body.addEventListener("onPlayFinished", HandlePlayFinishedEvent, false);
            document.body.addEventListener("onPauseEvent", HandlePauseEvent, false);
            document.body.addEventListener("onContinueEvent", HandleContinueEvent, false);
            document.body.addEventListener("onStopEvent", HandleStopEvent, false);
            document.body.addEventListener("onWin", HandleWinEvent, false);
            document.body.addEventListener("onLose", HandleLoseEvent, false);
            document.body.addEventListener("onMuteBtnClicked", HandleMuteBtnClicked, false);
            document.body.addEventListener("onPlaySoundsBtnClicked", HandlePlaySoundsBtnClicked, false);
            document.body.addEventListener("onMissionExit", HandleMissionExit, false);
            document.body.addEventListener("hideBGScene",hideBGScene,false);
            document.body.addEventListener("showBGScene",showBGScene,false);
            document.body.addEventListener("reOrienteCamera",reOrientCamera,false);
            document.body.addEventListener("enterDreamMode",enterDreamMode,false);
            document.body.addEventListener("exitDreamMode",exitDreamMode,false);
            document.body.addEventListener("enterEnviromentMode",enterEnviromentMode,false);
            document.body.addEventListener("exitEnviromentMode",exitEnviromentMode,false);
            document.body.addEventListener("speedUp",speedup,false);
            document.body.addEventListener("speedDown",speeddown,false);
        };

        this.InitializeScene = function(windowFocus, callback)
        {
            var sceneInitEvent = new CustomEvent("onSceneInitialize", {'detail': {
                assetspath:  rgVisualizer.GetAssetsPath(),
                windowFocus: windowFocus,
                callback: callback
            }});
            document.body.dispatchEvent(sceneInitEvent);
        };

        this.InitializeLoadingScene = function(callback)
        {
            var loadingSceneInitEvent = new CustomEvent("OnLoadingSceneInitialize",{'detail': {
                callback: callback
            }});
            document.body.dispatchEvent(loadingSceneInitEvent);
        };

        this.LoadMissionScene = function (callback){
            var sceneLoadEvent = new CustomEvent("onSceneLoad",{'detail': {
                parentdiv: rgVisualizer.GetParentDivID(),
                scenename: rgVisualizer.GetSceneName(),
                callback: callback
            }});
            document.body.dispatchEvent(sceneLoadEvent);
        };
        
        this.LoadMissionEnvironment = function(envScript, callBack){
            var gridInitEvent = new CustomEvent("onGridInit", {'detail': {
                motionscript: envScript,
                callback: callBack
            }});
            document.body.dispatchEvent(gridInitEvent);
        };

        this.RunMission = function(motionScript){
            RoboVisualizer.Run(motionScript);
        };

        this.PauseMission = function(){
            if(!RoboVisualizer.gameFlags.pauseBtnClicked)
            {
                RoboVisualizer.gameFlags.pauseBtnClicked = true;
                RoboVisualizer.gameFlags.playBtnClicked = false;
                RoboVisualizer.gameFlags.continueBtnClicked = false;
                RoboVisualizer.gameFlags.stopBtnClicked = false;
                var pauseEvent = new CustomEvent("onPauseEvent");
                document.body.dispatchEvent(pauseEvent);
            }
        };

        this.ContinueMission = function(){
           if(!RoboVisualizer.gameFlags.continueBtnClicked)
            {
                RoboVisualizer.gameFlags.pauseBtnClicked = false;
                RoboVisualizer.gameFlags.playBtnClicked = true;
                RoboVisualizer.gameFlags.continueBtnClicked = true;
                RoboVisualizer.gameFlags.stopBtnClicked = false;
                var continueEvent = new CustomEvent("onContinueEvent");
                document.body.dispatchEvent(continueEvent);
            }
        };

        this.StopMission = function(){
            if(!RoboVisualizer.gameFlags.stopBtnClicked)
            {
                RoboVisualizer.gameFlags.pauseBtnClicked = true;
                RoboVisualizer.gameFlags.playBtnClicked = false;
                RoboVisualizer.gameFlags.continueBtnClicked = true;
                RoboVisualizer.gameFlags.stopBtnClicked = true;
                var stopEvent = new CustomEvent("onStopEvent");
                document.body.dispatchEvent(stopEvent);
            }
        };

        this.RestartMission = function(){
            RoboVisualizer.Restart();
        };

        this.GetScore = function(){
            return RoboVisualizer.GetVisualizationResult();
        };

        this.PlayButtonClicked = function()
        {
            if(!RoboVisualizer.gameFlags.playBtnClicked)
            {
                RoboVisualizer.gameFlags.isStopped = false;
                RoboVisualizer.gameFlags.pauseBtnClicked = false;
                RoboVisualizer.gameFlags.playBtnClicked = true;
                RoboVisualizer.gameFlags.continueBtnClicked = true;
                RoboVisualizer.gameFlags.stopBtnClicked = false;
            }
        };

        this.CompileTestSuit = function(testSuit, callback){
            if(RoboVisualizer.gameFlags.playBtnClicked)
            {
                var playBtnClickedEvent = new CustomEvent("onPlayBtnClicked", {'detail': {
                    input: testSuit,
                    callback: callback
                }});
                document.body.dispatchEvent(playBtnClickedEvent);
            }
        };
        
        this.OnWin = function()
        {
            var onWinEvent = new CustomEvent("onWin");
            document.body.dispatchEvent(onWinEvent);
        };

        this.OnLose = function()
        {
            var onLoseEvent = new CustomEvent("onLose");
            document.body.dispatchEvent(onLoseEvent);
        };

        this.MuteSound = function()
        {
            var onMute = new CustomEvent("onMuteBtnClicked");
            document.body.dispatchEvent(onMute);
        };

        this.PlaySound = function()
        {
            var onPlay = new CustomEvent("onPlaySoundsBtnClicked");
            document.body.dispatchEvent(onPlay);
        };

        this.OnMissionExit = function()
        {
            var onMissionExitEvent = new CustomEvent("onMissionExit");
            document.body.dispatchEvent(onMissionExitEvent);
        };

        this.ClearScene = function()
        {
            RoboVisualizer.ClearScene();
        };

        this.WebglSupported = function()
        {
            return RoboVisualizer.WebglSupported();
        };

        this.hideBGScene = function(){
            var hideBGSceneEvent = new CustomEvent("hideBGScene");
            document.body.dispatchEvent(hideBGSceneEvent);
        };

        this.showBGScene = function(){
            var showBGSceneEvent = new CustomEvent("showBGScene");
            document.body.dispatchEvent(showBGSceneEvent);
        };

        this.reOrienteCamera = function(){
            var reOrienteCameraEvent = new CustomEvent("reOrienteCamera");
            document.body.dispatchEvent(reOrienteCameraEvent);
        };

        this.enterDreamMode = function(){
            var enterDreamModeEvent = new CustomEvent("enterDreamMode");
            document.body.dispatchEvent(enterDreamModeEvent);
        };
        this.exitDreamMode = function(){
            var exitDreamModeEvent = new CustomEvent("exitDreamMode");
            document.body.dispatchEvent(exitDreamModeEvent);
        };
        this.enterEnviromentMode = function(){
            var enterEnviromentModeEvent = new CustomEvent("enterEnviromentMode");
            document.body.dispatchEvent(enterEnviromentModeEvent);
        };
        this.exitEnviromentMode = function(){
            var exitEnviromentModeEvent = new CustomEvent("exitEnviromentMode");
            document.body.dispatchEvent(exitEnviromentModeEvent);
        };
        this.speedup = function(){
            var speedUpEvent = new CustomEvent("speedUp");
            document.body.dispatchEvent(speedUpEvent);
        };
        this.speeddown = function(){
            var speedDownEvent = new CustomEvent("speedDown");
            document.body.dispatchEvent(speedDownEvent);
        };

    })
    .directive('rgVisualizer', function($window, $timeout, $rootScope){
        return {
            scope: {},
            restrict: 'AE',
            replace: 'true',
            template: '<div id="divVisualizer" class="rg-visualizer" style="height:100%; width: 100%; overflow: hidden;"></div>'
    };
});
}());