README 1.20.0

* New Features:
    - OnWin Event
    - OnLose Event
    - MuteSound Event


README 1.19.0

* New Features:
    - new visualizer version 1.19.0 [Image Env]


README 1.18.0

* New Features:
    - new visualizer version 1.18.0


README 1.17.0

* New Features:
    - new visualizer version 1.17.0
* Bug Fixes:
    - stop then play then continue bug fix.


README 1.16.0

* Bug Fixes:
    - Pause bug fix

README 1.15.0

* Bug Fixes:
    - tab switch bug fix


README 1.14.0

* New Features:
    - PlayButtonClicked API

* Bug Fixes:
    - play button delay bug fix

README 1.13.0

* Updates
    - returning errors in callback of scene initialization functions

README 1.8.0

* Bug Fixes:
    - StopMission api bug fix
    - rename some variables to be more related.

    
README 1.7.0

* New Features:
    - new visualizer version 1.7.0
    - add WebglSupported api


README 1.6.0

* New Features:
    - new visualizer version "1.6.0"
    - add ClearScene api

README 1.5.0

* New Features:
    - added visualizer events handlers and initializations


README 1.4.0

* New Features:
    - new visualizer version "1.4.0"'
    - handle buttons flags

* Updates:
    - RestartMission function is no longer used


README 1.3.0

* New Features:
    - new visualizer core version 1.3.0


README 1.2.0

* New Features:
    - new visualizer core version 1.2.0


README 1.1.0

* New Features:
    - new visualizer core version 1.1.0

* Updates:
    - Migrating sceneInit into web app



README 1.0.0

* New Features:
    - set scene name before initializing visualizer

* Updates:
    - Init function
    - RunMission function
    - PauseMission function
    - ContinueMission function
    - RestartMission function
    - GetScore function
    - CompileTestSuit function

* Bug Fixes:
    - stop rendering when no movement is being proccessed